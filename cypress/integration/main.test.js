it("should load the page", () => {
  cy.visit("/");
});

it("cart should correct text if you don't have added elements", () => {
  cy.get("#cartEmptyMessage").should(
    "have.text",
    "Todavia no hay productos añadidos"
  );
});

it("should click in add to cart and button becomes disable after click", () => {
  cy.get("#productItem1 .btn-add-to-cart").click();
  cy.get("#productItem1 .btn-add-to-cart").should(
    "have.class",
    "without-stock"
  );
});

it("should cart show item added", () => {
  cy.get("#cartItem1 .description span").should(
    "have.text",
    "LaJusticia colágeno con magnesio 450comp"
  );
  cy.get("#cartItem1 .price span").should("have.text", "14.35€");
  cy.get("#cartItem1 .cart-cell-img img")
    .invoke("attr", "alt")
    .should("eq", "img-cart");
  cy.get("#cartItem1 .cart-cell-img img")
    .invoke("attr", "src")
    .should("eq", "/assets/images/lajusticia-colageno.jpg");
});
