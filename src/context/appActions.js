import React, { useReducer } from "react";

import data from "../services/db";
import AppContext from "./appContext";
import AppReducer from "./appReducer";

import { GET_PRODUCTS, ADD_TO_CART, CALCULATE_TOTAL } from "./types";

const AppActions = (props) => {
  let initialState = {
    products: [],
    cart_products: [],
    total_price: 0,
  };

  const [state, dispatch] = useReducer(AppReducer, initialState);

  const getProducts = async () => {
    dispatch({ type: GET_PRODUCTS, products: data });
  };

  const addToCart = (item) => {
    dispatch({
      type: ADD_TO_CART,
      cart_products: [...state.cart_products, item],
    });
  };

  const calculateTotal = (itemPrice) => {
    dispatch({
      type: CALCULATE_TOTAL,
      total_price: state.total_price + itemPrice,
    });
  };

  return (
    <AppContext.Provider
      value={{
        products: state.products,
        cart_products: state.cart_products,
        total_price: state.total_price,
        calculateTotal,
        getProducts,
        addToCart,
      }}
    >
      {props.children}
    </AppContext.Provider>
  );
};

export default AppActions;
