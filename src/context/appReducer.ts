import { GET_PRODUCTS, ADD_TO_CART, CALCULATE_TOTAL } from "./types";
import { Action } from "../types/action.type";
import { State } from "../types/state.type";

const AppReducer = (state: State, action: Action) => {
  const { products, cart_products, total_price, type } = action;
  switch (type) {
    case GET_PRODUCTS:
      return {
        ...state,
        products: products,
      };
    case ADD_TO_CART:
      return {
        ...state,
        cart_products: cart_products,
      };
    case CALCULATE_TOTAL:
      return {
        ...state,
        total_price: total_price,
      };
    default:
      return state;
  }
};

export default AppReducer;
