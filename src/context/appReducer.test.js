import expect from "expect";
import AppReducer from "./appReducer";
import { GET_PRODUCTS, ADD_TO_CART, CALCULATE_TOTAL } from "./types";

export const initialState = {
  products: [],
  cart_products: [],
  total_price: 0,
};

export const data = [
  {
    id: 1,
    description: "LaJusticia colágeno con magnesio 450comp",
    image_name: "lajusticia-colageno.jpg",
    price: 14.35,
  },
];

describe("app reducer", () => {
  it("should return the initial state", () => {
    expect(AppReducer(initialState, {})).toEqual(initialState);
  });

  it("should handle GET_PRODUCTS", () => {
    const stateExpect = {
      products: data,
    };

    const action = {
      type: GET_PRODUCTS,
      products: data,
    };
    expect(AppReducer({}, action)).toEqual(stateExpect);
  });

  it("should handle ADD_TO_CART", () => {
    const stateExpect = {
      cart_products: data,
    };

    const action = {
      type: ADD_TO_CART,
      cart_products: data,
    };
    expect(AppReducer({}, action)).toEqual(stateExpect);
  });

  it("should handle CALCULATE_TOTAL", () => {
    const stateExpect = {
      total_price: 100,
    };

    const action = {
      type: CALCULATE_TOTAL,
      total_price: 100,
    };
    expect(AppReducer({}, action)).toEqual(stateExpect);
  });
});
