/*
 * Define actions types
 */

export const GET_PRODUCTS = "GET_PRODUCTS";
export const ADD_TO_CART = "ADD_TO_CART";
export const CALCULATE_TOTAL = "CALCULATE_TOTAL";
