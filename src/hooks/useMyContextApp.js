/*
 * Custom hook to get current application context 
 */

import { useContext } from "react";
import AppContext from "../context/appContext";

const useContextApp = () => {
  const appState = useContext(AppContext);

  return { appState };
};

export default useContextApp;
