const data =  [
        {
            "id" : 1,
            "description": "LaJusticia colágeno con magnesio 450comp",
            "image_name": "lajusticia-colageno.jpg",
            "price": 14.35
        },
        {
            "id" : 2,
            "description": "Xhekpon® cream facial 40ml",
            "image_name": "xhekpon-crema.jpg",
            "price": 6.49
        },
        {
            "id" : 3,
            "description": "Cerave® Crema Hidratante 340ml",
            "image_name": "cerave-crema.jpg",
            "price": 11.70
        },
        {
            "id" : 4,
            "description": "Hyabak solución 10ml",
            "image_name": "hyabak-solucion.jpg",
            "price": 9.48
        },
        {
            "id" : 5,
            "description": "Fotoprotector ISDIN® Fusion Water SPF 50+ 50ml",
            "image_name": "fotoprotector-isdin.jpg",
            "price": 19.74
        },
        {
            "id" : 6,
            "description": "Piz Buin® Allergy SPF50+ loción 200ml",
            "image_name": "piz-buin.jpg",
            "price": 14.35
        }
    ]

export default data