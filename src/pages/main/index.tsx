import React from "react";
import Cart from "../../components/cart";
import Products from "../../components/product";

const Main = () => {
  return (
    <div className="containter">
      <Products />
      <Cart />
    </div>
  );
};

export default Main;
