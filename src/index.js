import React from "react";
import ReactDOM from "react-dom";
import "../src/styles/index.css";
import StoreProvider from "./store";
import Main from "./pages/main";

ReactDOM.render(
  <StoreProvider>
    <Main />
  </StoreProvider>,
  document.getElementById("root")
);
