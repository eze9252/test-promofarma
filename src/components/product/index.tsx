/*
 * Product component
 */

import React, { useEffect } from "react";
import ProductTable from "./components/productTable";
import useContextApp from "../../hooks/useMyContextApp";

const Products = () => {
  const { appState } = useContextApp();

  useEffect(() => {
    appState.getProducts();
  }, []);

  return <ProductTable />;
};

export default Products;
