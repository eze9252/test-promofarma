import React from "react";
import Product from "./product";
import useContextApp from "../../../../hooks/useMyContextApp";
import { Item } from "../../../../types/item.type";

const ProductTable = () => {
  const { appState } = useContextApp();

  const addToCart = (item: Item) => {
    appState.addToCart(item);
    appState.calculateTotal(item.price);
  };

  const isItemAddedToCart = (id: number) =>
    appState.cart_products.some((item: Item) => item.id === id);

  return (
    <div className="product-list">
      {appState.products.map((item: Item) => {
        return (
          <Product
            key={item.id}
            item={item}
            addToCart={addToCart}
            isItemAddedToCart={isItemAddedToCart(item.id)}
          />
        );
      })}
    </div>
  );
};

export default ProductTable;
