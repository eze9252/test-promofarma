import React from "react";
import { Item } from "../../../../types/item.type";

const Product = ({
  item,
  addToCart,
  isItemAddedToCart,
}: {
  key: number;
  item: Item;
  addToCart: Function;
  isItemAddedToCart: boolean;
}) => {
  return (
    <div id={`productItem${item.id}`} className="row-product-flex">
      <div className="product-cell description">
        <span>{item.description}</span>
      </div>
      <div className="product-cell price">
        <span className="price">{item.price}€</span>
      </div>
      <div className="product-cell">
        <img
          alt="img-button"
          onClick={() => addToCart(item)}
          className={
            isItemAddedToCart
              ? "btn-add-to-cart without-stock"
              : "btn-add-to-cart"
          }
          src={`${process.env.PUBLIC_URL}/assets/icons/add-to-cart.png`}
          width="50"
          height="35"
        />
      </div>
    </div>
  );
};

export default Product;
