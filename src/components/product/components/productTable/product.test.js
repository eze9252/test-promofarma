import { render, screen } from "@testing-library/react";
import Product from "./product";

test("render Product Component item and button add to cart is disable", () => {
  const item = {
    id: 1,
    description: "LaJusticia colágeno con magnesio 450comp",
    image_name: "lajusticia-colageno.jpg",
    price: 14.35,
  };

  render(<Product item={item} isItemAddedToCart={true} />);
  expect(screen.getByRole("img")).toHaveClass("btn-add-to-cart without-stock");
});

test("render Product Component item and button add to cart is enable", () => {
  const item = {
    id: 1,
    description: "LaJusticia colágeno con magnesio 450comp",
    image_name: "lajusticia-colageno.jpg",
    price: 14.35,
  };

  render(<Product item={item} isItemAddedToCart={false} />);
  expect(screen.getByRole("img")).toHaveClass("btn-add-to-cart");
});

test("render Product Component correctly when there are no items", () => {
  const tree = render(<Product item={{}} />);
  expect(tree).toMatchSnapshot();
});
