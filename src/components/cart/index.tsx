/*
 * Cart component
 */

import React, { useEffect, useState } from "react";
import CartItem from "./components/cartDetails/cartItem";
import useContextApp from "../../hooks/useMyContextApp";
import { Item } from "../../types/item.type";

const Cart = () => {
  const [cartItems, setCartItems] = useState([]);
  const [totalPrice, setTotalPrice] = useState(0);
  const { appState } = useContextApp();

  useEffect(() => {
    setCartItems(appState.cart_products);
    setTotalPrice(appState.total_price.toFixed(2));
  }, [appState]);

  return (
    <div className="cart-container">
      <div>
        <p className="cart-title">MI CESTA</p>
      </div>
      {cartItems.length ? (
        cartItems.map((item: Item) => {
          return <CartItem key={item.id} item={item} />;
        })
      ) : (
        <p id="cartEmptyMessage" className="row-cart-flex">
          Todavia no hay productos añadidos
        </p>
      )}
      <div className="cart-resume">
        <div className="total-container">
          <span className="total">TOTAL </span>
          <span> ({cartItems.length} productos)</span>
        </div>
        <div className="price">{totalPrice}€</div>
      </div>
    </div>
  );
};

export default Cart;
