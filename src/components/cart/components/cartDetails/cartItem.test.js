import { render, screen } from "@testing-library/react";
import CartItem from "./cartItem";

test("render CartItem Component correctly when there are no items", () => {
  const tree = render(<CartItem item={{}} />);
  expect(tree).toMatchSnapshot();
});

test("render CartItem Component item and img has the right attributes", () => {
  const item = {
    id: 1,
    description: "LaJusticia colágeno con magnesio 450comp",
    image_name: "lajusticia-colageno.jpg",
    price: 14.35,
  };

  render(<CartItem item={item} isItemAddedToCart={true} />);
  const image = screen.getByAltText("img-cart");
  expect(image.width).toBe(75);
  expect(image.height).toBe(75);
  expect(image.src).toBeDefined();
});
