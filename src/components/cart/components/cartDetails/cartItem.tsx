import React from "react";
import { Item } from "../../../../types/item.type";

const CartItem = ({ item }: { key: number; item: Item }) => {
  return (
    <div id={`cartItem${item.id}`} className="row-cart-flex">
      <div className="cart-cell-img">
        <img
          alt="img-cart"
          src={`${process.env.PUBLIC_URL}/assets/images/${item.image_name}`}
          width="75"
          height="75"
        />
      </div>
      <div className="cart-cell description">
        <span>{item.description}</span>
      </div>
      <div className="cart-cell price">
        <span>{item.price}€</span>
      </div>
    </div>
  );
};

export default CartItem;
