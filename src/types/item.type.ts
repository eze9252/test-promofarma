export type Item = {
  id: number;
  description: string;
  image_name: number[];
  price: number;
};
