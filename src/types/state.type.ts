export type State = {
    cart_products: any;
    products: any;
    total_price: number;
};
  