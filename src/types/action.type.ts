export type Action = {
  type: string;
  products?: any
  cart_products?: any;
  total_price?: number;
};
