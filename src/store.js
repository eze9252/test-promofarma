import React from "react";
import AppActions from "./context/appActions";

const createStoreProvider =
  (providers) =>
  ({ children }) =>
    providers
      .reverse()
      .reduce((tree, Provider) => <Provider>{tree}</Provider>, children);

const StoreProvider = createStoreProvider([AppActions]);

export default StoreProvider;
